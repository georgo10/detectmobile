<?php

/**
 *
 * Detection for mobile device and redirecting
 * user to mobile/smart/tablet version
 *
 * @author Tomas Kopecny <tomas@kopecny.info>
 * @version 1.0
 * @copyright 2007 - 2013 Tomas Kopecny
 * @url http://git.kopecny.info/detectmobile
 * @license http://www.gnu.org/licenses/lgpl.html GNU Lesser General Public License
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA
 *     02111-1307, USA.
 *
 * Changelog:
 *             2013-10-26 - Added detection of tablet and smart phones, changed logic of cookie
 *             2009-06-10 - Added detection for Opera Mobile 9.x (HTC branded)
 *             2008-07-17 - Added detection for Opera Mobile 9.5 beta
 *             2008-07-16 - Added UA for Google Mobile Wrapper
 */


/**
 * Detect mobile device
 *
 * @description If is mobile device detected, cookie set for performance for next detecting
 *
 * @param bool ignoreForce Ignore force cookie to detect real device
 * @return bool/string Information about detected mobile device - string is type of device, FALSE is browser
 */
function detect_mobile_device($ignoreCookie = false)
{

	// Initiate detection of mobile device
	$mobile_ua = strtolower(substr($_SERVER['HTTP_USER_AGENT'],0,4));
	$mobile_agents = array('acs-','alav','alca','amoi','audi','aste','avan','benq','bird','blac','blaz','brew','cell','cldc','cmd-','dang','doco','eric','g10-', 'hipt','htc_','inno','ipaq','java','jigs','kddi','keji','leno','lg-c','lg-d','lg-g','lge-','maui','maxo','midp','mits','mmef','mobi','mot-','moto','mwbp','nec-','newt','noki','opwv','palm','pana','pant','pdxg','phil','play','pluc','port','prox','qtek','qwap','sage','sams','sany','sch-','sec-','send','seri','sgh-','shar','sie-','siem','smal','smar','sony','sph-','symb','t-mo','teli','tim-','tosh','tsm-','upg1','upsi','vk-v','voda','wap-','wapa','wapi','wapp','wapr','webc','winw','winw','xda','xda-');
	if ((isset($_COOKIE['isbrowser'])) && ($_COOKIE['isbrowser']=='1') && $ignoreCookie == false) { // Force browser version
		return false;
	} elseif (
		(strpos(strtolower($_SERVER['HTTP_ACCEPT']),'text/vnd.wap.wml')!==false) || 
		(strpos(strtolower($_SERVER['HTTP_ACCEPT']),'application/vnd.wap.xhtml+xml')!==false) || 
		(isset($_SERVER['HTTP_X_WAP_PROFILE'])) || 
		(isset($_SERVER['HTTP_PROFILE'])) || 
		(isset($_SERVER['HTTP_X_OPERAMINI_FEATURES'])) || 
		(isset($_SERVER['HTTP_UA_PIXELS'])) ||
		(in_array($mobile_ua,$mobile_agents)) ||
		((preg_match('/(up.browser|up.link|windows ce|iemobile|mmp|symbian|smartphone|midp|wap|phone|vodafone|pocket|mobile|pda|psp|htc_|android[; ])/i',$_SERVER['HTTP_USER_AGENT'])) &&
		(!preg_match('/(windows nt \d|mac os x \d)/i', $_SERVER['HTTP_USER_AGENT'])))
	) {

		if(preg_match('#(tablet|ipad;|android 3)#i', $_SERVER['HTTP_USER_AGENT']) || (stripos($_SERVER['HTTP_USER_AGENT'], 'android')!==false && stripos($_SERVER['HTTP_USER_AGENT'], 'mobile')===false && stripos($_SERVER['HTTP_USER_AGENT'],'Opera Mini')===false)) {
			return 'tablet';
		}
		elseif(preg_match('#(webos/|iphone;|ipad;|ipod;|android[; ]+([2-9]|mobile)|bada/|htc_)#i', $_SERVER['HTTP_USER_AGENT'])) {
			return 'smart';
		}
		return 'wap';
	}
	if(!headers_sent()) // For better performance on next detection
	{
		setcookie('isbrowser', 1, time()+3600*24*31, '/', '.yourdomain.com');
	}
	return FALSE;
};
